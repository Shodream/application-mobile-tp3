package com.example.tp3_saladin;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.nfc.Tag;
import android.os.AsyncTask;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private SimpleCursorAdapter cursorAdaptater = null;
    private static final int SECOND_ACTIVITY_REQUEST_CODE = 0;
    SwipeRefreshLayout refresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        refresh = (SwipeRefreshLayout)findViewById(R.id.swiperefresh);
        refresh.setColorSchemeColors(getResources().getColor(android.R.color.holo_blue_bright), getResources().getColor(android.R.color.holo_green_light), getResources().getColor(android.R.color.holo_orange_light), getResources().getColor(android.R.color.holo_red_light));

        refresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        updateList();
                    }
                }
        );


        final ListView myList = (ListView) findViewById(R.id.listView);
        final WeatherDbHelper weatherDbHelper = new WeatherDbHelper(this);
        weatherDbHelper.clearDataBase();
        weatherDbHelper.populate();

        Cursor cursor = weatherDbHelper.fetchAllCities();

        String [] columns = new String[] {WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.COLUMN_COUNTRY};

        cursorAdaptater = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2,cursor,columns, new int[] { android.R.id.text1, android.R.id.text2});
        myList.setAdapter(cursorAdaptater);

        myList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Cursor cursor = (Cursor) cursorAdaptater.getItem(position);
                Intent intent = new Intent(MainActivity.this, CityActivity.class);

                City city = weatherDbHelper.cursorToCity(cursor);
                intent.putExtra("city",city);

                startActivity(intent);
            }
        });



        FloatingActionButton buttonAdd = findViewById(R.id.buttonAdd);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewCityActivity.class);
                startActivityForResult(intent, SECOND_ACTIVITY_REQUEST_CODE);
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // check that it is the SecondActivity with an OK result
        if (requestCode == SECOND_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) { // Activity.RESULT_OK
                City city = (City) data.getExtras().get("city");
                final WeatherDbHelper weatherDbHelper = new WeatherDbHelper(this);
                weatherDbHelper.addCity(city);
                cursorAdaptater.swapCursor(weatherDbHelper.fetchAllCities());
                cursorAdaptater.notifyDataSetChanged();
            }
        }
    }
    @SuppressLint("StaticFieldLeak")
    public void updateList() {
        final WeatherDbHelper weatherDbHelper = new WeatherDbHelper(this);
        final ArrayList<City> listCity = weatherDbHelper.getAllCities();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute(){
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {

                for(City city : listCity){
                    try{
                        Log.d(TAG,"ville == "+city);
                        URL url = WebServiceUrl.build(city.getName(), city.getCountry());
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        JSONResponseHandler JSresp = new JSONResponseHandler(city);
                        JSresp.readJsonStream(connection.getInputStream());
                        weatherDbHelper.updateCity(city);

                    } catch (Exception e){

                        System.out.println("Exception "+ e.getMessage());
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void Void)
            {
                refresh.setRefreshing(false);
                cursorAdaptater.swapCursor(weatherDbHelper.fetchAllCities());
                cursorAdaptater.notifyDataSetChanged();
            }


        }.execute();
    }
}
