package com.example.tp3_saladin;
import com.example.tp3_saladin.City;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class CityActivity extends AppCompatActivity {

    private static final String TAG = CityActivity.class.getSimpleName();
    private TextView textCityName, textCountry, textTemperature, textHumdity, textWind, textCloudiness, textLastUpdate;
    private ImageView imageWeatherCondition;
    private City city;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        city = (City) getIntent().getExtras().get("city");
        Log.d("DEBUG", "la ville = " + city.getName());

        textCityName = (TextView) findViewById(R.id.nameCity);
        textCountry = (TextView) findViewById(R.id.country);
        textTemperature = (TextView) findViewById(R.id.editTemperature);
        textHumdity = (TextView) findViewById(R.id.editHumidity);
        textWind = (TextView) findViewById(R.id.editWind);
        textCloudiness = (TextView) findViewById(R.id.editCloudiness);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageWeatherCondition = (ImageView) findViewById(R.id.imageView);

        updateView();

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ConnectivityManager manager = (ConnectivityManager) getSystemService(CityActivity.CONNECTIVITY_SERVICE);
                NetworkInfo infoNetwork = manager.getActiveNetworkInfo();

                if(infoNetwork != null) {
                   updateWeather();
                }
            }
        });

    }
    @SuppressLint("StaticFieldLeak")
    public void updateWeather() {
        new AsyncTask<Void, Void, Void>(){
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    URL url = WebServiceUrl.build(city.getName(), city.getCountry());
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    JSONResponseHandler JSresp = new JSONResponseHandler(city);
                    JSresp.readJsonStream(connection.getInputStream());
                } catch (Exception e) {
                    System.out.println("Exception "+ e.getMessage());
                    return null;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void Void) {
                try {
                    String infoCity = city.getName() + ", " +
                            city.getCountry() + ", " +
                            city.getTemperature() + ", " +
                            city.getHumidity() + ", " +
                            city.getWindSpeed() + ", " +
                            city.getWindDirection() + ", " +
                            city.getCloudiness() + ", " +
                            city.getIcon() + ", " +
                            city.getDescription() + ", " +
                            city.getIcon() + ", " +
                            city.getDescription() + ", " +
                            city.getLastUpdate();

                    String icon = "icon_" + city.getIcon();

                    textTemperature.setText(city.getTemperature() + "°");
                    textHumdity.setText(city.getHumidity() + "%");
                    textWind.setText(city.getWindSpeed() + "km/h (" + city.getWindDirection() + ")");
                    textCloudiness.setText(city.getCloudiness() + "%");
                    textLastUpdate.setText(city.getLastUpdate());

                    Resources res = CityActivity.this.getResources();
                    int resID = res.getIdentifier(icon, "drawable", getPackageName());
                    imageWeatherCondition.setImageResource(resID);

                }
                catch (Exception e){

                    System.out.println("Exception "+ e.getMessage());
                }


            }
        }.execute();
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("city", city);
        setResult(RESULT_OK, intent);
        finish();
        super.onBackPressed();
    }

    private void updateView() {

        textCityName.setText(city.getName());
        textCountry.setText(city.getCountry());
        textTemperature.setText(city.getTemperature()+" °C");
        textHumdity.setText(city.getHumidity()+" %");
        textWind.setText(city.getFullWind());
        textCloudiness.setText(city.getHumidity()+" %");
        textLastUpdate.setText(city.getLastUpdate());

        if (city.getIcon()!=null && !city.getIcon().isEmpty()) {
            Log.d(TAG,"icon="+"icon_" + city.getIcon());
            imageWeatherCondition.setImageDrawable(getResources().getDrawable(getResources()
                    .getIdentifier("@drawable/"+"icon_" + city.getIcon(), null, getPackageName())));
            imageWeatherCondition.setContentDescription(city.getDescription());
        }

    }
}
